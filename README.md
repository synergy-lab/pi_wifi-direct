Equipment:
1.	2-9 raspberry Pis 
2.	HDMI to DVI/VGA cable
3.	2-9 micro usb cable
4.	2-9 8GB micro sd card
5.	USB hubs 
6.	Raspberry pi cases  
7.	X box controller

Install OS: 
First download the raspbian package from the raspberry pi website and put it on the sd card. Then insert sd card to the raspberry pi and power it on. Follow the instruction and complete the OS installation. 

Step One: 
Download the wifi_configure.sh script from bitbucket repository. And run the script. It will install and update necessary packages and configure the network setting.  

Step Two(Only Pi1): 
After the wifi_configure.sh script has been successfully run. 
For Pi1, download the wpa_suplicant.conf and startWifiDirect script from the repository. 
Modify the network internface: 
$ sudo cp /etc/network/interfaces /etc/network/interfaces.bak
$ sudo vi /etc/network/interfaces
delete anything these two lines: 
auto lo 
iface lo inet loopback
$ sudo mv /etc/wpa_supplicant/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant.conf.bak
Then place the downloaded wpa_supplicant.conf in /etc/wpa_supplicant/
$ sudo cd /path/to/downloaded/wpa_supplicant.conf
$ sudo cp wpa_supplicant.conf /etc/wpa_supplicant.conf
Then disable the network manager 
$sudo update-rc.d dhcpcd disable
Restart the pi 
$ sudo reboot 

Step 3(Just for pi1): 

after the devices has been successfully rebooted, run startwifiDirect script. (it needs to be run everytime when the devices has been restarted)
$ chmod +x startWifiDirect
$ ./startWifiDirect
if the wifi-direct is initiated successfully 
then create a wifi direct group 
$ sudo wpa_cli
$ p2p_group_add 
Record the wifi name and passphrase for the wifi group.

Step 4 
Connect pi 2-9 to the wifi group created in the previous group 
Change the ip address the each device to 192.168.3.(100+x), where x is the device number (1-9)

Step 5 
On Pi1, run the generateSSHKey script for each other pis that are connected to the wifi group. and edit the /etc/hosts file so that it contains the name and IP address of each other nodes. Modify the mpihostsfile in the home directory and add the ip addresses of each nodes to it. 

Step 6
The TinyTitan_Wifi-Direct should be ready to go. Just cd to SPH directory and type make run. It will launch the SPG Program via MPI.